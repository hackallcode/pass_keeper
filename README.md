# PassKeeper

This is a simple server to store passwords and other sensitive data.

## How to run

1. Install Python 3.6 or higher
2. Run `python main.py`
