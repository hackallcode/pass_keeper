from pass_keeper.keeper import Keeper


def main():
    keeper = Keeper(host='127.0.0.1', port='3545', debug=False)
    keeper.run()


if __name__ == '__main__':
    main()
