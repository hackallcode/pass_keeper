import datetime
import os
from collections import namedtuple
from functools import wraps

import flask
import jwt

from pass_keeper.models import DB

UserData = namedtuple('UserData', ['username', 'secret', 'exp'])
PassData = namedtuple('PassData', ['name', 'data'])


class Keeper:
    def __init__(self, host=None, port=None, debug=None):
        self.__host = host
        self.__port = port

        self.__jwt_secret = 'VERY_SECRET_JWT_KEY_123'

        self.__app = flask.Flask(__name__, static_url_path='/static')
        self.__app.secret_key = os.urandom(24)
        self.__app.debug = debug

        self.__init_handlers()

        self.__db = DB()

    def run(self):
        self.__app.run(self.__host, self.__port)

    def __add_handler(self, rule, handler, **options):
        self.__app.route(rule, **options)(handler)

    def __init_handlers(self):
        self.__add_handler('/', self.auth_middleware(self.home), methods=['GET'])
        self.__add_handler('/login', self.auth_middleware(self.login), methods=['GET', 'POST'])
        self.__add_handler('/register', self.auth_middleware(self.register), methods=['GET', 'POST'])
        self.__add_handler('/logout', self.logout)
        self.__add_handler('/add', self.auth_middleware(self.data_add), methods=['GET', 'POST'])
        self.__add_handler('/get', self.auth_middleware(self.data_get))

    def auth_middleware(self, func):
        @wraps(func)
        def decorated_function(*args, **kwargs):
            user = None
            jwt_payload = flask.request.cookies.get('jwt')
            if jwt_payload:
                try:
                    user = UserData(**jwt.decode(jwt_payload, self.__jwt_secret, algorithms=['HS256']))
                except Exception:
                    pass

            return func(user=user, *args, **kwargs)

        return decorated_function

    def home(self, user):
        names = None
        if user:
            names = self.__db.data_get_names(user.username)
        return flask.render_template('home.html', user=user, names=names)

    def login(self, user):
        if user:
            return flask.make_response(flask.redirect('/'))

        username = None
        if flask.request.method == 'POST':
            username = flask.request.form.get('username')
            password = flask.request.form.get('password')
            secret = flask.request.form.get('secret')
            if self.__db.user_login(username, password, secret):
                jwt_payload = jwt.encode({
                    'username': username,
                    'secret': secret,
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=1),
                }, self.__jwt_secret)

                resp = flask.make_response(flask.redirect('/'))
                resp.set_cookie('jwt', jwt_payload, httponly=True, secure=not self.__app.debug)
                return resp
            else:
                flask.flash('Wrong username, password or secret!', "danger")
        return flask.render_template('login.html', username=username)

    def register(self, user):
        if user:
            return flask.make_response(flask.redirect('/'))

        username = None
        if flask.request.method == 'POST':
            username = flask.request.form.get('username')
            password = flask.request.form.get('password')
            secret = self.__db.user_add(username, password)
            if secret:
                new_user = {
                    'username': username,
                    'secret': secret,
                    'exp': datetime.datetime.utcnow() + datetime.timedelta(hours=1),
                }
                jwt_payload = jwt.encode(new_user, self.__jwt_secret)

                resp = flask.make_response(flask.render_template('secret.html', user=new_user))
                resp.set_cookie('jwt', jwt_payload, httponly=True, secure=not self.__app.debug)
                return resp
            else:
                flask.flash('User already exists!', 'info')
        return flask.render_template('register.html', username=username)

    def logout(self):
        response = flask.make_response(flask.redirect('/login'))
        response.delete_cookie('jwt')
        return response

    def data_add(self, user: UserData):
        if not user:
            return flask.make_response(flask.redirect('/login'))

        if flask.request.method == 'POST':
            name = flask.request.form.get('name')
            data = flask.request.form.get('data')
            if self.__db.data_add(user.username, user.secret, name, data):
                return flask.redirect('/')
            else:
                flask.flash('Impossible to upload data!', 'info')
        return flask.render_template('data_add.html', user=user)

    def data_get(self, user: UserData):
        if not user:
            return flask.make_response(flask.redirect('/login'))

        pass_data = None
        name = flask.request.args.get('name')
        if name:
            data = self.__db.data_get(user.username, user.secret, name)
            if data:
                pass_data = PassData(name=name, data=data)
            else:
                flask.flash('Impossible to get data!', 'warning')
        return flask.render_template('data_get.html', user=user, data=pass_data)
