import bcrypt
from cryptography.fernet import Fernet
from peewee import (
    Model,
    SqliteDatabase,
    PostgresqlDatabase,
    AutoField,
    CharField, ForeignKeyField, TextField,
)


class DB:
    def __init__(
        self,
        db_name=None,
        db_file=None,
        db_user='pass_keeper',
        db_password='pass_keeper',
        secret_test_data=b'SuperSecretTestData!',
    ):
        if db_name is None:
            if db_file is None:
                db_file = 'pass_keeper.db'
            db = SqliteDatabase(db_file)
        else:
            db = PostgresqlDatabase(db_name, user=db_user, password=db_password)

        class BaseModel(Model):
            class Meta:
                database = db

        class User(BaseModel):
            id = AutoField()
            username = CharField(unique=True)
            pass_hash = CharField()
            test_data = TextField()

        class Data(BaseModel):
            id = AutoField()
            user = ForeignKeyField(User)
            name = CharField()
            data = TextField()

        self.User = User
        self.User.create_table()

        self.Data = Data
        self.Data.create_table()

        self.__secret_test_data = secret_test_data

    def user_get(self, username: str):
        return self.User.get_or_none(self.User.username == username.lower().strip())

    def user_add(self, username: str, password: str):
        if self.user_get(username) is not None:
            return None

        pass_hash = bcrypt.hashpw(password.encode(), bcrypt.gensalt())
        secret = Fernet.generate_key()
        cipher = Fernet(secret)
        encrypted_text = cipher.encrypt(self.__secret_test_data)

        user = self.User(
            username=username.lower().strip(),
            pass_hash=pass_hash,
            test_data=encrypted_text.decode(),
        )
        user.save()

        return secret.decode()

    def user_login(self, login: str, password: str, secret: str):
        user = self.user_get(login)
        if user is None:
            return None

        if not bcrypt.checkpw(password.encode(), user.pass_hash.encode()):
            return None

        try:
            assert Fernet(secret.encode()).decrypt(user.test_data.encode()) == self.__secret_test_data
        except Exception:
            return None

        return user

    def data_add(self, username: str, secret: str, name: str, data: str):
        user = self.user_get(username)
        if user is None:
            return None

        cipher = Fernet(secret.encode())
        encrypted_data = cipher.encrypt(data.encode()).decode()
        db_data = self.Data(
            user=user,
            name=name,
            data=encrypted_data,
        )
        return db_data.save()

    def data_get_names(self, username: str):
        user = self.user_get(username)
        if user is None:
            return None

        data_list = self.Data.select(self.Data.name).where(self.Data.user == user)
        names = []
        for data in data_list:
            names.append(data.name)
        return names

    def data_get(self, username: str, secret: str, name: str):
        user = self.user_get(username)
        if user is None:
            return None

        data = self.Data.get_or_none(self.Data.user == user, self.Data.name == name)
        if data is None:
            return None

        try:
            cipher = Fernet(secret.encode())
            return cipher.decrypt(data.data.encode()).decode()
        except Exception:
            pass
