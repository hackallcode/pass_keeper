let copiable_elements = document.getElementsByClassName('copy');
Array.prototype.forEach.call(copiable_elements, element => {
    element.onclick = function () {
        document.execCommand("copy");
    }

    element.addEventListener("copy", function (event) {
        event.preventDefault();
        if (event.clipboardData) {
            event.clipboardData.setData("text/plain", element.textContent);
        }
    });
});
